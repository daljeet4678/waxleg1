//
//  GameScene.swift
//  Test34-S19
//
//  Created by MacStudent on 2019-06-19.
//  Copyright © 2019 rabbit. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var leg:SKSpriteNode!
    var hair1:SKSpriteNode!
    var hair2:SKSpriteNode!
    var hair3: SKSpriteNode!
    var hair4: SKSpriteNode!
    var nextLevelButton:SKLabelNode!
    
    
    override func didMove(to view: SKView) {
        self.hair1 = self.childNode(withName:"hair1") as! SKSpriteNode
        print("This is level 1")
        self.nextLevelButton = self.childNode(withName: "nextLevelButton") as! SKLabelNode
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        if (touch == nil) {
            return
        }
        
        
        let location = touch!.location(in:self)
        var touchedNode = self.atPoint(location)
        
        if let name = touchedNode.name {
            if (name == "hair1") {
                print("Player touched hair1")
            }
                else
                if (name == "hair2") {
                print("Player touched hair2")
                
            }
                else
                    if (name == "hair3") {
                        print("Player touched hair3")
            }
                    else
                        if (name == "hair4") {
                            print("Player touched hair4")
            
                touchedNode = touchedNode as! SKSpriteNode
                
        
            
                
            }
        
        // MARK: Switch Levels
        if (touchedNode.name == "nextLevelButton") {
            let scene = SKScene(fileNamed:"Level2")
            if (scene == nil) {
                print("Error loading level")
                return
            }
            else {
                scene!.scaleMode = .aspectFill
                view?.presentScene(scene!)
            }
        }
        
    }
}

}
